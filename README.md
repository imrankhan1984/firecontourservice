# README #

This is the simple Fire Contour Service that accepts data from Java SunSpots on its URI http://127.0.0.1:8080/FireContourcService/webresources/fire

### What is this repository for? ###

The SunSpots POST their readings at the above URI, which is then forwarded to the associated JXTA peer running on the same machine.

The main page of the service is http://127.0.0.1:8080/FireContourcService

The SunSpots POST their data on http://127.0.0.1:8080/FireContourcService/webresources/fire


### How do I get set up? ###

You need a working Apache Tomcat server on your machine. There are two ways to use Fire Contour.

1 - Get the WAR file from the **dist** folder of the repository and use Tomcat manager-gui to deploy it. 

2 - Checkout the whole source code of the repository and open the project in NetBeans (all required files are included). Compile and Run the code.


### Contribution guidelines ###

If you are making changes to the code then please don't forget to **push** the updated code. 

### Who do I talk to? ###

contact: imrankhan1984@gmail.com