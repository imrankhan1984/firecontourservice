/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fireservice;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author umroot
 */
@Path("fire")
public class FireContourService {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ServiceResource
     */
    public FireContourService() {
    }

    /**
     * Retrieves representation of an instance of com.semService.FireContourService
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("text/html")
    public String getHtml() {
        return "<h1>Welcome to Fire Monitoring Service!!<h1>";
        //throw new UnsupportedOperationException();
    }

/*    @PUT
    @Consumes("application/senml+json")
    @Produces("text/plain")
    public void putHtml(String content) {
        
    }*/
    
    @POST
        @Consumes("text/plain")
        public Response postHtml(String content) throws UnknownHostException, IOException {
            Response str = SensorDataSender(content);
            return str;
        }
    
    public Response SensorDataSender(String message) throws IOException {    

            try (Socket cSocket = new Socket("localhost", 6789)) {
                DataOutputStream outToServer = new DataOutputStream(cSocket.getOutputStream());
                outToServer.writeBytes(message + '\n');
               // BufferedReader inFromServer = new BufferedReader(new InputStreamReader(cSocket.getInputStream()));
               // reply = inFromServer.readLine();
            }
          return Response.status(Response.Status.CREATED.getStatusCode()).build();
        } 
    
}
